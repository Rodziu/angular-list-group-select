/*
 * AngularJS & Bootstrap list-group-select plugin.
 * Copyright (c) 2016-2018 Rodziu <mateusz.rohde@gmail.com>
 * License: MIT
 */

!function(){
	'use strict';
	const app = angular.module('angularListGroupSelect', []);
	/**
	 * List group select options
	 */
	app.provider('listGroupSelect', function(){
		this.options = {
			lang: {
				selectListTitle: 'Opcje',
				selectedListTitle: 'Wybrane Opcje',
				search: 'Szukaj...',
				selectAll: 'Wybierz wszystko',
				deselectAll: 'Odznacz wszystko'
			},
			sortable: false
		};
		// noinspection JSUnusedGlobalSymbols
		this.$get = function(){
			return this.options;
		};
	});
	/**
	 * @ngdoc component
	 * @name listGroupSelect
	 *
	 * @param {expression|array} ngModel
	 * @param {expression|array} options
	 * @param {expression|string} displayColumn
	 * @param {expression|boolean} sortable
	 * @param {string} selectListTitle
	 * @param {string} selectedListTitle
	 * @param {string} search
	 * @param {string} selectAll
	 * @param {string} deselectAll
	 */
	app.component('listGroupSelect', {
		require: {
			ngModelCtrl: 'ngModel'
		},
		bindings: {
			options: '<',
			ngModel: '=',
			column: '<?displayColumn',
			sortable: '<?'
		},
		controllerAs: 'ctrl',
		templateUrl: 'src/templates/list-group-select.ng',
		controller: ['$scope', '$attrs', 'listGroupSelect', function($scope, $attrs, listGroupSelect){
			const ctrl = this;
			/**
			 */
			ctrl.$onInit = function(){
				ctrl.lang = {};
				for(let l in listGroupSelect.lang){
					if(listGroupSelect.lang.hasOwnProperty(l)){
						ctrl.lang[l] = l in $attrs ? $attrs[l] : listGroupSelect.lang[l];
					}
				}
				if(typeof ctrl.sortable === 'undefined'){
					ctrl.sortable = listGroupSelect.sortable;
				}
				if(!Array.isArray(ctrl.ngModel)){
					ctrl.ngModel = [];
				}
				/**
				 * @var {{}} ctrl.ngModelCtrl
				 */
				ctrl.ngModelCtrl.$isEmpty = function(value){
					return !value || value.length === 0;
				};
			};
			/**
			 */
			$scope.$watch('ctrl.options', function(){
				if(!Array.isArray(ctrl.ngModel)){
					return;
				}
				for(let s = ctrl.ngModel.length - 1; s > -1; s--){
					let match = false;
					for(let o = 0; o < ctrl.options.length; o++){
						if(angular.equals(ctrl.options[o], ctrl.ngModel[s])){
							match = true;
							break;
						}
					}
					if(!match){
						ctrl.ngModel.splice(s, 1);
					}
				}
			}, true);
			/**
			 * @param item
			 */
			ctrl.select = function(item){
				if(!Array.isArray(ctrl.ngModel)){
					ctrl.ngModel = [];
				}
				ctrl.ngModel.push(item);
				ctrl.ngModelCtrl.$setTouched();
			};
			/**
			 * @param item
			 */
			ctrl.deselect = function(item){
				ctrl.ngModel.splice(ctrl.ngModel.indexOf(item), 1);
			};
			/**
			 */
			ctrl.selectAll = function(){
				angular.extend(ctrl.ngModel, ctrl.options);
			};
			/**
			 */
			ctrl.deselectAll = function(){
				ctrl.ngModel = [];
			};
			/**
			 */
			ctrl.isAvailable = function(){
				return function(item){
					if(typeof ctrl.ngModel === 'undefined'){
						return true;
					}
					return !~ctrl.ngModel.indexOf(item);
				};
			};
			/**
			 * @param direction
			 * @param $index
			 */
			ctrl.sort = function(direction, $index){
				if(
					direction === 'up' && $index === 0
					|| direction === 'down' && $index >= ctrl.ngModel.length - 1
				){
					return;
				}
				const otherIndex = direction === 'up' ? $index - 1 : $index + 1,
					tmp = ctrl.ngModel[otherIndex];
				ctrl.ngModel[otherIndex] = ctrl.ngModel[$index];
				ctrl.ngModel[$index] = tmp;
			};
		}]
	});
	/**
	 * @ngdoc filter
	 * @name listGroupSelectSearch
	 */
	app.filter("listGroupSelectSearch", [function(){
		const hasSearchString = function(variable, search){
			if(typeof variable === 'object'){
				for(let v in variable){
					if(
						variable.hasOwnProperty(v)
						&& v !== "$$hashKey"
						&& hasSearchString(variable[v], search)
					){
						return true;
					}
				}
			}else{
				// noinspection EqualityComparisonWithCoercionJS
				if(
					variable == search
					|| (
						typeof variable.toLowerCase === 'function'
						&& !!~variable.toLowerCase().indexOf(search)
					)
				){
					return true;
				}
			}
			return false;
		};
		return function(options, search){
			if(typeof search === 'undefined' || !search){
				return options;
			}
			const ret = [];
			search = search.toLowerCase();
			for(let o = 0; o < options.length; o++){
				if(hasSearchString(options[o], search)){
					ret.push(options[o]);
				}
			}
			return ret;
		}
	}]);
}();
angular.module('angularListGroupSelect').run(['$templateCache', function($templateCache) {$templateCache.put('src/templates/list-group-select.ng','<div class="row"><div class="col-sm-6"><div class="panel panel-default"><div class="panel-heading"><h4 class="panel-title">{{::ctrl.lang[\'selectListTitle\']}}</h4></div><div class="panel-body"><input type="text" class="form-control input-sm" ng-model="ctrl.search" placeholder="{{::ctrl.lang[\'search\']}}..."></div><div class="list-group list-group-select"><a ng-repeat="item in ctrl.filteredOptions = (ctrl.options | filter:ctrl.isAvailable() | listGroupSelectSearch:ctrl.search | orderBy:ctrl.column)" class="list-group-item" ng-click="ctrl.select(item)">{{ctrl.column && item[ctrl.column] != undefined ? item[ctrl.column] : item}}</a></div><div class="panel-footer"><button type="button" class="btn btn-sm btn-info" data-ng-disabled="!ctrl.filteredOptions.length" ng-click="ctrl.selectAll()">{{::ctrl.lang[\'selectAll\']}}</button> <button type="button" class="btn btn-sm btn-info" data-ng-disabled="!ctrl.ngModel.length" ng-click="ctrl.deselectAll()">{{::ctrl.lang[\'deselectAll\']}}</button></div></div></div><div class="col-sm-6"><div class="panel panel-default"><div class="panel-heading"><h4 class="panel-title">{{::ctrl.lang[\'selectedListTitle\']}}</h4></div><div class="list-group list-group-select list-group-select-selected"><div class="list-group-item" ng-repeat="item in ctrl.ngModel | orderBy:ctrl.sortable ? \'\' : ctrl.column" ng-class="{\'no-click\': ctrl.sortable}" ng-click="!ctrl.sortable ? ctrl.deselect(item) : \'\'"><span class="pull-right" ng-if="ctrl.sortable"><a ng-click="ctrl.sort(\'down\', $index)" ng-class="{\'no-click\': $last}"><i class="fa fa-fw" ng-class="{\'fa-angle-double-down\': !$last}"></i></a> <a ng-click="ctrl.sort(\'up\', $index)" ng-class="{\'no-click\': $first}"><i class="fa fa-fw" ng-class="{\'fa-angle-double-up\': !$first}"></i></a> <a ng-click="ctrl.deselect(item)"><i class="fa fa-fw fa-times text-danger"></i></a></span> {{ctrl.column && item[ctrl.column] != undefined ? item[ctrl.column] : item}}</div></div></div></div></div>');}]);