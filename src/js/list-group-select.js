/*
 * AngularJS & Bootstrap list-group-select plugin.
 * Copyright (c) 2016-2018 Rodziu <mateusz.rohde@gmail.com>
 * License: MIT
 */

!function(){
	'use strict';
	const app = angular.module('angularListGroupSelect', []);
	/**
	 * List group select options
	 */
	app.provider('listGroupSelect', function(){
		this.options = {
			lang: {
				selectListTitle: 'Opcje',
				selectedListTitle: 'Wybrane Opcje',
				search: 'Szukaj...',
				selectAll: 'Wybierz wszystko',
				deselectAll: 'Odznacz wszystko'
			},
			sortable: false
		};
		// noinspection JSUnusedGlobalSymbols
		this.$get = function(){
			return this.options;
		};
	});
	/**
	 * @ngdoc component
	 * @name listGroupSelect
	 *
	 * @param {expression|array} ngModel
	 * @param {expression|array} options
	 * @param {expression|string} displayColumn
	 * @param {expression|boolean} sortable
	 * @param {string} selectListTitle
	 * @param {string} selectedListTitle
	 * @param {string} search
	 * @param {string} selectAll
	 * @param {string} deselectAll
	 */
	app.component('listGroupSelect', {
		require: {
			ngModelCtrl: 'ngModel'
		},
		bindings: {
			options: '<',
			ngModel: '=',
			column: '<?displayColumn',
			sortable: '<?'
		},
		controllerAs: 'ctrl',
		templateUrl: 'src/templates/list-group-select.ng',
		controller: ['$scope', '$attrs', 'listGroupSelect', function($scope, $attrs, listGroupSelect){
			const ctrl = this;
			/**
			 */
			ctrl.$onInit = function(){
				ctrl.lang = {};
				for(let l in listGroupSelect.lang){
					if(listGroupSelect.lang.hasOwnProperty(l)){
						ctrl.lang[l] = l in $attrs ? $attrs[l] : listGroupSelect.lang[l];
					}
				}
				if(typeof ctrl.sortable === 'undefined'){
					ctrl.sortable = listGroupSelect.sortable;
				}
				if(!Array.isArray(ctrl.ngModel)){
					ctrl.ngModel = [];
				}
				/**
				 * @var {{}} ctrl.ngModelCtrl
				 */
				ctrl.ngModelCtrl.$isEmpty = function(value){
					return !value || value.length === 0;
				};
			};
			/**
			 */
			$scope.$watch('ctrl.options', function(){
				if(!Array.isArray(ctrl.ngModel)){
					return;
				}
				for(let s = ctrl.ngModel.length - 1; s > -1; s--){
					let match = false;
					for(let o = 0; o < ctrl.options.length; o++){
						if(angular.equals(ctrl.options[o], ctrl.ngModel[s])){
							match = true;
							break;
						}
					}
					if(!match){
						ctrl.ngModel.splice(s, 1);
					}
				}
			}, true);
			/**
			 * @param item
			 */
			ctrl.select = function(item){
				if(!Array.isArray(ctrl.ngModel)){
					ctrl.ngModel = [];
				}
				ctrl.ngModel.push(item);
				ctrl.ngModelCtrl.$setTouched();
			};
			/**
			 * @param item
			 */
			ctrl.deselect = function(item){
				ctrl.ngModel.splice(ctrl.ngModel.indexOf(item), 1);
			};
			/**
			 */
			ctrl.selectAll = function(){
				angular.extend(ctrl.ngModel, ctrl.options);
			};
			/**
			 */
			ctrl.deselectAll = function(){
				ctrl.ngModel = [];
			};
			/**
			 */
			ctrl.isAvailable = function(){
				return function(item){
					if(typeof ctrl.ngModel === 'undefined'){
						return true;
					}
					return !~ctrl.ngModel.indexOf(item);
				};
			};
			/**
			 * @param direction
			 * @param $index
			 */
			ctrl.sort = function(direction, $index){
				if(
					direction === 'up' && $index === 0
					|| direction === 'down' && $index >= ctrl.ngModel.length - 1
				){
					return;
				}
				const otherIndex = direction === 'up' ? $index - 1 : $index + 1,
					tmp = ctrl.ngModel[otherIndex];
				ctrl.ngModel[otherIndex] = ctrl.ngModel[$index];
				ctrl.ngModel[$index] = tmp;
			};
		}]
	});
	/**
	 * @ngdoc filter
	 * @name listGroupSelectSearch
	 */
	app.filter("listGroupSelectSearch", [function(){
		const hasSearchString = function(variable, search){
			if(typeof variable === 'object'){
				for(let v in variable){
					if(
						variable.hasOwnProperty(v)
						&& v !== "$$hashKey"
						&& hasSearchString(variable[v], search)
					){
						return true;
					}
				}
			}else{
				// noinspection EqualityComparisonWithCoercionJS
				if(
					variable == search
					|| (
						typeof variable.toLowerCase === 'function'
						&& !!~variable.toLowerCase().indexOf(search)
					)
				){
					return true;
				}
			}
			return false;
		};
		return function(options, search){
			if(typeof search === 'undefined' || !search){
				return options;
			}
			const ret = [];
			search = search.toLowerCase();
			for(let o = 0; o < options.length; o++){
				if(hasSearchString(options[o], search)){
					ret.push(options[o]);
				}
			}
			return ret;
		}
	}]);
}();