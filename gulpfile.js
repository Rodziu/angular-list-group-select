/*
 * Angular extended select element plugin for AngularJS.
 * Copyright (c) 2016-2018 Rodziu <mateusz.rohde@gmail.com>
 * License: MIT
 */
const pkg = require('./package'),
	gulp = require('gulp'),
	concat = require('gulp-concat'),
	minifyHTML = require('gulp-minify-html'),
	templateCache = require('gulp-angular-templatecache'),
	rename = require('gulp-rename'),
	uglify = require('gulp-uglify-es').default,
	sass = require('gulp-sass'),
	sourcemaps = require('gulp-sourcemaps'),
	cssMin = require('gulp-clean-css');

gulp.task('templates', function(){
	return gulp.src('src/templates/**.ng')
		.pipe(minifyHTML({quotes: true}))
		.pipe(templateCache('templates.js', {
			module: 'angularListGroupSelect',
			root: 'src/templates'
		}))
		.pipe(gulp.dest('dist/js'));
});

gulp.task('js', ['templates'], function(){
	return gulp.src(['src/js/**/*.js', 'dist/js/templates.js'])
		.pipe(concat(pkg.name + '.js'))
		.pipe(gulp.dest('dist/js'))
		.pipe(rename(pkg.name + '.min.js'))
		.pipe(sourcemaps.init())
		.pipe(uglify({
			output: {
				preamble: '/*! ' + pkg.description + ' v.' + pkg.version +
					' | (c) 2016-' + (new Date()).getFullYear() + ' ' + pkg.author + ' */'
			}
		}))
		.pipe(sourcemaps.write('./', {includeContent: false}))
		.pipe(gulp.dest('dist/js'))
		.on('end', function(){
			require('fs').unlinkSync('dist/js/templates.js');
			return this;
		});
});

gulp.task('css', function(){
	return gulp.src('src/scss/*.scss')
		.pipe(sass())
		.pipe(gulp.dest('dist/css'))
		.pipe(rename(pkg.name + '.min.css'))
		.pipe(sourcemaps.init())
		.pipe(cssMin())
		.pipe(sourcemaps.write('./', {includeContent: false}))
		.pipe(gulp.dest('dist/css'));
});

gulp.task('default', ['js', 'css']);