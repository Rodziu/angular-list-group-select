/*
 * AngularJS & Bootstrap list-group-select plugin.
 * Copyright (c) 2016-2018 Rodziu <mateusz.rohde@gmail.com>
 * License: MIT
 */
!function(){
	'use strict';
	const app = angular.module('exampleApp', ['angularListGroupSelect']);
	app.controller('exampleCtrl', ['$scope', '$http', function($scope, $http){
		$scope.options = [];
		$scope.model = [];
		$scope.isSortable = false;
		$scope.displayColumn = 'email';
		$http.get('mock_data.json').then(function(response){
			$scope.options = response.data;
			$scope.model = [angular.copy($scope.options[0])];
		});
		$scope.splice = function(){
			$scope.options.splice(0, 1);
		};
		$scope.changeFn = function(){
			console.log($scope.model);
		};
	}]);
}();