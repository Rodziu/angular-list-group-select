# AngularJS & Bootstrap list-group-select plugin

Multiple select variation with use of  [Twitter Bootstrap](https://getbootstrap.com/docs/3.3/) list-group.

See more at [Live Demo](https://mateuszrohde.pl/repository/angular-list-group-select/demo/index.html)

## Prerequisites

- AngularJS,
- Twitter Bootstrap,
- Font Awesome (sorting icons).

## Installation

```
yarn add angular-list-group-select
yarn install
```

## Usage

```
<list-group-select options="options" ng-model="model"></list-group-select>
```

Where `options` is an array of possible options.

You can change the display column with the use of `display-column="columnName"` directive.

There is also an option to enable selected options sorting with the use of `sortable="true|false"` directive.

You can change the default language strings in `listGroupSelect` provider during config phase or by providing them as attributes, eg.:

```
<list-group-select options="options" ng-model="model" select-list-title="Select list title"></list-group-select>
```